#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace
from pathlib import Path
import shutil
from loguru import logger

from convert_md_to_html import main as main_convert_md_to_html
from generate_index import main as main_generate_index

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("input_dir", type=Path)
    parser.add_argument("output_dir", type=Path)
    parser.add_argument("--theme_path", required=True, type=Path, help="Path to the theme directory")
    parser.add_argument("--overwrite", action="store_true", help="Overwrite the html dir")
    args = parser.parse_args()
    assert not args.output_dir.exists() or args.overwrite, f"'{args.output_dir}' exists. Use --overwrite"
    return args

def main(args: Namespace):
    """main fn"""
    if args.output_dir.exists() and args.overwrite:
        logger.info(f"Removing '{args.output_dir}'")
        shutil.rmtree(args.output_dir)
    args.output_dir.mkdir(parents=True, exist_ok=True)
    for md_path in [x for x in args.input_dir.iterdir() if x.suffix == ".md"]:
        convert_args = Namespace(
            in_md_path=md_path,
            out_html_path=args.output_dir / f"{md_path.stem}.html",
            theme_path=args.theme_path,
            overwrite=True,
            title=md_path.stem,
        )
        main_convert_md_to_html(convert_args)

    generate_index_args = Namespace(
        html_dir=args.output_dir,
        theme_path=args.theme_path,
        overwrite=True,
    )
    main_generate_index(generate_index_args)

if __name__ == "__main__":
    main(get_args())
