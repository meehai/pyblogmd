#!/usr/bin/env python3
from pathlib import Path
from argparse import ArgumentParser, Namespace
from copy import copy

import markdown
import bs4
from loguru import logger

from utils import get_title_and_date_from_md
from theme import Theme

def _get_all_lists(html_content: str) -> list[list[tuple[int, int]]]:
    """
    Get all list entries, that is a '\n-' and the next '\n'
    If there is more text between the two, it means that the 2nd entry is part of a new list.
    """
    first = html_content.find("\n- ")
    if first == -1:
        return []
    last = html_content.find("\n", first + 1)
    assert last != -1, "Found a list entry, but no newline after it"
    res = []
    current = []
    while first != -1:
        current.append((first + 1, last))
        first = html_content.find("\n- ", last)
        if first != last:
            res.append(current)
            current = []
        last = html_content.find("\n", first + 1)
        assert last != -1, "Found a list entry, but no newline after it"
    if len(current) > 0:
        res.append(current)
    return res

def _tag_all_lists(html_content: str) -> str:
    """convert all lists to <ul> <li> ... </li> ... </ul>"""
    list_pairs = _get_all_lists(html_content)
    curr_accum = 0 # Accumulated offset due to adding <ul> and <li> tags
    for curr_list in list_pairs:
        assert len(curr_list) > 0
        len_before = len(html_content)
        left, right = curr_list[0][0] + curr_accum, curr_list[-1][1] + curr_accum
        curr_list_str = html_content[left: right]
        curr_list_str = f"<ul>{curr_list_str.replace('- ', '<li>')}</ul>"
        html_content = f"{html_content[0:left]}{curr_list_str}{html_content[right:]}"
        curr_accum += len(html_content) - len_before
    return html_content

def _convert_raw_md(md_content: str) -> str:
    md = markdown.Markdown(extensions=["mdx_math", "fenced_code", "codehilite", "pymdownx.emoji"])
    html_content = md.convert(md_content)
    html_content = _tag_all_lists(html_content)
    return html_content

def convert_md_to_html(md_content: str, theme: Theme) -> str:
    """takes 1 md file and converts it to html using the theme provided, plus timestamp, date and title"""
    title, date, ix = get_title_and_date_from_md(md_content)
    soup = theme.bs4_content
    # Convert the md to html and parse it
    html_content = _convert_raw_md(md_content[ix:])
    html_text = bs4.BeautifulSoup(html_content, "html.parser")
    # Remove the hr tag that is added by markdown
    html_text.find("hr").decompose()
    # Add the content to the theme
    div_content = soup.find("div", class_="content_text")
    div_content.string = ""
    div_content.append(html_text)
    # Add the date as provided1 in
    div_timestamp = soup.find("div", class_="content_timestamp")
    div_timestamp.string = ""
    div_timestamp.append(date.strftime("%Y-%m-%d"))
    # Add the blog title as provided
    div_title = soup.find("div", class_="content_title")
    div_title.string = ""
    div_title.append(title)
    return f"{soup}"

def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("in_md_path", type=Path, help="Path ot the markdown input file")
    parser.add_argument("out_html_path", type=Path, help="Path to store the html file.")
    parser.add_argument("--theme_path", required=True, type=Path, help="Path to the theme file")
    parser.add_argument("--overwrite", action="store_true", help="Overwrite the html file if it exists")
    args = parser.parse_args()
    assert not args.out_html_path.exists() or args.overwrite, f"'{args.out_html_path}' exists. Use --overwrite"
    return args

def main(args: Namespace):
    md_content = open(args.in_md_path, "r").read()
    assert args.in_md_path.stem == get_title_and_date_from_md(md_content)[0], \
        f"Filename '{args.in_md_path.stem}' does not match title '{get_title_and_date_from_md(md_content)[0]}'"
    logger.info(f"Converting markdown file '{args.in_md_path}' ({len(md_content)} characters) to html")
    theme = Theme(args.theme_path)
    html_result = convert_md_to_html(md_content, theme)
    args.out_html_path.parent.mkdir(exist_ok=True, parents=True)
    open(args.out_html_path, "w").write(html_result)
    logger.info(f"Written html file '{args.out_html_path}' ({len(html_result)} characters)")

if __name__ == "__main__":
    main(get_args())
