"""utils for pyblogmd"""
import bs4
from datetime import datetime, date

def get_title_and_date_from_md(md_content: str) -> tuple[str, date, int]:
    """Get the title and date from the md file. Also returns the index after the comments, so they can be skipped"""
    assert md_content[0:4] == "---\n", "The markdown file must start with '---'"
    ix = md_content[4:].find("---\n", 4) + 4
    assert ix != -1, "The markdown file must have at least 2 '---' separators plus title and date inside"
    md_meta = md_content[:ix].split("\n")
    date_str, title = md_meta[2][6:], md_meta[1][7:]
    date = datetime.fromisoformat(date_str)
    assert md_meta[1].startswith("title: ") and md_meta[2].startswith("date: "), \
        f"The first 4 lines of the markdown file must be the metadata, got '{md_meta[:4]}'"
    assert title != "index", "Title cannot be 'index'"
    ix = md_content[4:].find("---\n", 4) + 4
    assert ix != -1, "The markdown file must have at least 2 '---' separators plus title and date inside"
    return title, date, ix

def get_title_and_date_from_html(html_content: str) -> tuple[str, date]:
    """gets the title and date from the html file. Must be called on a generated html file from markdown blog post"""
    soup = bs4.BeautifulSoup(html_content, "html.parser")
    title = soup.find("div", class_="content_title").string
    date_str = soup.find("div", class_="content_timestamp").string
    date = datetime.fromisoformat(date_str)
    assert title is not None and date is not None, "Probably a badly generated html file"
    return title, date
