"""theme module"""
from pathlib import Path
from loguru import logger
import bs4

class Theme:
    """Theme class for blog"""
    def __init__(self, theme_path: Path):
        self.theme_path = theme_path
        self.html_content = open(theme_path / "content.html", "r").read()
        self.html_index = open(theme_path / "index.html", "r").read()
        self.css = open(theme_path / "style.css", "r").read()
        self.bs4_content = self._build_bs4_content()
        self.bs4_index = self._build_bs4_index()
        logger.info(f"Loaded blog theme from '{theme_path}'")

    def _build_bs4_content(self) -> bs4.BeautifulSoup:
        """bs4 content html + css applied"""
        soup = bs4.BeautifulSoup(self.html_content, "html.parser")
        # Add the style to the theme
        soup.head.append(style := soup.new_tag("style"))
        style.string = self.css
        for expected_tag in ["content_text", "content_title", "content_timestamp", "global_title"]:
            assert soup.find("div", class_=expected_tag) is not None, f"Cannot find '{expected_tag}' div"
        return soup

    def _build_bs4_index(self) -> bs4.BeautifulSoup:
        """bs4 index html + css applied"""
        soup = bs4.BeautifulSoup(self.html_index, "html.parser")
        # Add the style to the theme
        soup.head.append(style := soup.new_tag("style"))
        style.string = self.css
        for expected_tag in ["index_list", "index_entry", "index_entry_date", "index_entry_title", "global_title"]:
            assert soup.find("div", class_="index_list") is not None, "Cannot find 'index_list' div"
        return soup
