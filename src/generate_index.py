#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace
from pathlib import Path
from loguru import logger

from utils import get_title_and_date_from_html
from theme import Theme

def generate_index_html(html_files: list[Path], theme: Theme) -> str:
    """
    Generate the index.html file from the theme and the list of md files. One a href is generated for each md file
    We need to get to this structure:
    <div class="index_list">
        <a href="content.html">
            <div class="index_entry">
                <div class="index_entry_date">2023-01-01</div>
                <div class="index_entry_title">content</div>
            </div>
        </a>
        ...
    </div>

    """
    assert isinstance(html_files, list) and len(html_files) > 0, html_files
    assert all(html_file.suffix == ".html" for html_file in html_files), html_files
    soup = theme.bs4_index

    # clear the dummy index_list from the theme
    div_content = soup.find("div", class_="index_list")
    div_content.string = ""
    # collect all the new entries
    all_new_entries = []
    for html_file in html_files:
        title, date = get_title_and_date_from_html(open(html_file, "r").read())
        # This href must be aligned well with the md to html conversion. For now we use blog_name.html, which is fine.
        a_blog_entry = soup.new_tag("a", href=html_file.name)
        div_index_entry = soup.new_tag("div", attrs={"class": "index_entry"})
        div_index_entry_date = soup.new_tag("div", attrs={"class": "index_entry_date"})
        div_index_entry_date.string = date.strftime("%Y-%m-%d")
        div_index_entry_title = soup.new_tag("div", attrs={"class": "index_entry_title"})
        div_index_entry_title.string = title
        div_index_entry.extend([div_index_entry_date, div_index_entry_title])
        a_blog_entry.append(div_index_entry)
        all_new_entries.append(a_blog_entry)
    # sort them and add them to the index_list
    all_new_entries = sorted(all_new_entries, key=lambda x: x.find(class_="index_entry_date").string, reverse=True)
    for entry in all_new_entries:
        div_content.append(entry)
    return f"{soup}"

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("html_dir", type=Path, help="Path to the html dir where store the index file.")
    parser.add_argument("--theme_path", required=True, type=Path, help="Path to the theme file")
    parser.add_argument("--overwrite", action="store_true", help="Overwrite the html file if it exists")
    args = parser.parse_args()
    index_html_file = args.html_dir / "index.html"
    assert args.html_dir.exists(), f"Out path '{args.html_dir}' doesn't exist"
    assert not index_html_file.exists() or args.overwrite, f"'{index_html_file}' exists. Use --overwrite"
    return args

def main(args: Namespace):
    """main fn"""
    index_html_file = args.html_dir / "index.html"
    if args.overwrite and index_html_file.exists():
        logger.info(f"Removing '{index_html_file}'")
        index_html_file.unlink()
    theme = Theme(args.theme_path)
    html_files = [x for x in args.html_dir.iterdir() if x.suffix == ".html"]
    assert "index.html" not in html_files, "html files cannot be named 'index.html'"
    assert len(html_files) > 0, f"At least one html file must be present in '{args.html_dir}"
    index_str = generate_index_html(html_files, theme)
    open(index_html_file, "w").write(index_str)
    logger.info(f"Generated index file '{index_html_file}' with {len(html_files)} entries")

if __name__ == "__main__":
    main(get_args())
