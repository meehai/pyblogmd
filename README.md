# PyBlogMd

Generate static html files from markdown scripts (with math included). Useful to create a blog of markdown files
and then convert these to html files.

## Usage

- Convert md to html script:
```
python convert.py md_dir html_dir [--overwrite]
```

This generates one html file for each md file in the `md/` directory plus one `index.html` file that will list
all the blog entries.

- Run http server
```
python -m http.server html/ 8080
```

All of this can be achieved using `bash run.sh 8080` as well.

## TODOs:
- proper styling -- we use a basic styles.css
- proper theme -- we use a hardcoded html 'theme'
