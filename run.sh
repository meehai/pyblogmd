#!/usr/bin/bash
set -e

python src/convert_all.py md_blog html --theme_path theme/ --overwrite
python -m http.server --directory html/ $1
